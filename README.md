# Verify-Signal-APK

Compares the SHA-256 of the "Signer #1" certificate of a downloaded .apk file and confirms it matches that of Whisper Systems (Signal) organisation.

USAGE:

verify-signal-apk /path/to/signal.apk

(script will also conveniently output this help when run without being passed a properly existing file's path)


Very quick upload prompted from spontaneous need - improvements and enhancements welcomed!